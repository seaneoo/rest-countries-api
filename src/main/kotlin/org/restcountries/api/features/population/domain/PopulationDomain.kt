package org.restcountries.api.features.population.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.restcountries.api.features.country.Country
import org.restcountries.api.features.population.Population

@Serializable
data class PopulationDomain(
	@SerialName("country_code") val countryCode: String,
	val year: String,
	@SerialName("total_pop") val totalPopulation: String,
	@SerialName("male_pop") val malePopulation: String,
	@SerialName("female_pop") val femalePopulation: String
)

fun PopulationDomain.entity(country: Country) = Population(
	populationYear = this.year,
	totalPopulation = this.totalPopulation.toDoubleOrNull() ?: 0.0,
	malePopulation = this.malePopulation.toDoubleOrNull() ?: 0.0,
	femalePopulation = this.femalePopulation.toDoubleOrNull() ?: 0.0,
	country = country
)
