package org.restcountries.api.features.population

import jakarta.persistence.*
import org.restcountries.api.features.country.Country
import org.restcountries.api.features.population.dto.PopulationDto

@Entity
@Table(name = "populations")
data class Population(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 4, nullable = false, unique = false) val populationYear: String,
	@Column(nullable = false, unique = false) val totalPopulation: Double,
	@Column(nullable = false, unique = false) val malePopulation: Double,
	@Column(nullable = false, unique = false) val femalePopulation: Double,
	@ManyToOne @JoinColumn(
		name = "countryCode", referencedColumnName = "m49Code"
	) val country: Country
)

fun Population.domain() = PopulationDto(
	year = this.populationYear,
	totalPopulation = this.totalPopulation,
	malePopulation = this.malePopulation,
	femalePopulation = this.femalePopulation,
	m49Code = this.country.m49Code
)
