package org.restcountries.api.features.population

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface PopulationRepository : JpaRepository<Population, Int> {
	@Query(value = "SELECT * FROM populations WHERE country_code = :countryCode", nativeQuery = true)
	fun findPopulations(countryCode: String): List<Population>
}
