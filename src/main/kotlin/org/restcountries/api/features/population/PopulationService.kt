package org.restcountries.api.features.population

import org.springframework.stereotype.Service

@Service
class PopulationService(private val populationRepository: PopulationRepository) {
	fun findPopulations(countryCode: String) = populationRepository.findPopulations(countryCode)

	fun saveAll(populations: Iterable<Population>): List<Population> = populationRepository.saveAll(populations)
}
