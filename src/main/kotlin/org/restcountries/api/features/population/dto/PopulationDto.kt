package org.restcountries.api.features.population.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class PopulationDto(
	val year: String,
	@JsonProperty("total_population") val totalPopulation: Double,
	@JsonProperty("male_population") val malePopulation: Double,
	@JsonProperty("female_population") val femalePopulation: Double,
	@JsonProperty("country_m49_code") val m49Code: String
) : RepresentationModel<PopulationDto>()
