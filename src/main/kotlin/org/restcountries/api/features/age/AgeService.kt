package org.restcountries.api.features.age

import org.springframework.stereotype.Service

@Service
class AgeService(private val ageRepository: AgeRepository) {
	fun findAges(countryCode: String) = ageRepository.findAges(countryCode)

	fun saveAll(ages: Iterable<Age>): List<Age> = ageRepository.saveAll(ages)
}
