package org.restcountries.api.features.age

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AgeRepository : JpaRepository<Age, Int> {
	@Query(value = "SELECT * FROM ages WHERE country_code = :countryCode", nativeQuery = true)
	fun findAges(countryCode: String): List<Age>
}
