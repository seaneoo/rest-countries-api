package org.restcountries.api.features.age.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.restcountries.api.features.age.Age
import org.restcountries.api.features.country.Country

@Serializable
data class AgeDomain(
	@SerialName("country_code") val countryCode: String,
	val year: String,
	@SerialName("median_age") val medianAge: String
)

fun AgeDomain.entity(country: Country) = Age(
	ageYear = this.year, medianAge = this.medianAge.toDoubleOrNull() ?: 0.0, country = country
)
