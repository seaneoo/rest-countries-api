package org.restcountries.api.features.age

import jakarta.persistence.*
import org.restcountries.api.features.age.dto.AgeDto
import org.restcountries.api.features.country.Country

@Entity
@Table(name = "ages")
data class Age(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 4, nullable = false, unique = false) val ageYear: String,
	@Column(nullable = false, unique = false) val medianAge: Double,
	@ManyToOne @JoinColumn(
		name = "countryCode", referencedColumnName = "m49Code"
	) val country: Country
)

fun Age.domain() = AgeDto(
	year = this.ageYear, medianAge = this.medianAge, m49Code = this.country.m49Code
)
