package org.restcountries.api.features.age.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class AgeDto(
	val year: String,
	@JsonProperty("median_age") val medianAge: Double,
	@JsonProperty("country_m49_code") val m49Code: String
) : RepresentationModel<AgeDto>()
