package org.restcountries.api.features.country

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CountryRepository : JpaRepository<Country, Int> {
	fun findByM49Code(m49Code: String): Optional<Country>

	@Query(value = "SELECT * FROM countries WHERE subregion_code = :subregionCode", nativeQuery = true)
	fun findCountries(subregionCode: String): List<Country>
}
