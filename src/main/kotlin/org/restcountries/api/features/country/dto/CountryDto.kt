package org.restcountries.api.features.country.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class CountryDto(
	@JsonProperty("m49_code") val m49Code: String,
	@JsonProperty("alpha2_code") val a2Code: String,
	@JsonProperty("alpha3_code") val a3Code: String,
	val name: String,
	@JsonProperty("region_code") val subregionCode: String,
	val flags: FlagDto
) : RepresentationModel<CountryDto>()
