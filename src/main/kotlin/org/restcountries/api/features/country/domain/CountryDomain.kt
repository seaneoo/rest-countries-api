package org.restcountries.api.features.country.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.restcountries.api.features.country.Country

@Serializable
data class CountryDomain(
	val name: String,
	@SerialName("m49_code") val m49Code: String,
	@SerialName("a2_code") val a2Code: String,
	@SerialName("a3_code") val a3Code: String,
	@SerialName("subregion_code") val subregionCode: String
)

fun CountryDomain.entity() = Country(
	name = this.name,
	m49Code = this.m49Code,
	a2Code = this.a2Code,
	a3Code = this.a3Code,
	subregionCode = this.subregionCode,
	flagPng = "https://flagcdn.com/w320/${this.a2Code.lowercase()}.png",
	flagSvg = "https://flagcdn.com/${this.a2Code.lowercase()}.svg"
)
