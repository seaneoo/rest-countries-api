package org.restcountries.api.features.country

import jakarta.persistence.*
import org.restcountries.api.features.country.dto.CountryDto
import org.restcountries.api.features.country.dto.FlagDto

@Entity
@Table(name = "countries")
data class Country(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 3, nullable = false, unique = true) val m49Code: String,
	@Column(length = 2, nullable = false, unique = true) val a2Code: String,
	@Column(length = 3, nullable = false, unique = true) val a3Code: String,
	@Column(length = 100, nullable = false, unique = true) val name: String,
	@Column(length = 3, nullable = false, unique = false) val subregionCode: String,
	@Column(length = 32, nullable = false, unique = true) val flagPng: String? = null,
	@Column(length = 27, nullable = false, unique = true) val flagSvg: String? = null
)

fun Country.domain() = CountryDto(
	m49Code = this.m49Code,
	a2Code = this.a2Code,
	a3Code = this.a3Code,
	name = this.name,
	subregionCode = this.subregionCode,
	flags = FlagDto(png = this.flagPng, svg = this.flagSvg)
)
