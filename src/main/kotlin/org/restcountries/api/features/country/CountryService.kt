package org.restcountries.api.features.country

import org.restcountries.api.features.age.Age
import org.restcountries.api.features.age.AgeService
import org.restcountries.api.features.birth.Birth
import org.restcountries.api.features.birth.BirthService
import org.restcountries.api.features.death.Death
import org.restcountries.api.features.death.DeathService
import org.restcountries.api.features.population.Population
import org.restcountries.api.features.population.PopulationService
import org.springframework.stereotype.Service
import kotlin.jvm.optionals.getOrNull

@Service
class CountryService(
	private val countryRepository: CountryRepository,
	private val populationService: PopulationService,
	private val ageService: AgeService,
	private val birthService: BirthService,
	private val deathService: DeathService
) {
	fun findAll(): List<Country> = countryRepository.findAll()

	fun findByM49Code(m49Code: String): Country? = countryRepository.findByM49Code(m49Code).getOrNull()

	fun findCountries(subregionCode: String): List<Country> = countryRepository.findCountries(subregionCode)

	fun findPopulations(m49Code: String): List<Population> = populationService.findPopulations(m49Code)

	fun findAges(m49Code: String): List<Age> = ageService.findAges(m49Code)

	fun findBirths(m49Code: String): List<Birth> = birthService.findBirths(m49Code)

	fun findDeaths(m49Code: String): List<Death> = deathService.findDeaths(m49Code)

	fun saveAll(countries: Iterable<Country>): List<Country> = countryRepository.saveAll(countries)
}
