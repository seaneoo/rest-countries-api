package org.restcountries.api.features.country.dto

data class FlagDto(
	val png: String? = null, val svg: String? = null
)
