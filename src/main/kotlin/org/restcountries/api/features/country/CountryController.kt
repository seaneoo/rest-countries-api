package org.restcountries.api.features.country

import org.restcountries.api.LinkRelations
import org.restcountries.api.features.age.domain
import org.restcountries.api.features.age.dto.AgeDto
import org.restcountries.api.features.birth.domain
import org.restcountries.api.features.birth.dto.BirthDto
import org.restcountries.api.features.country.dto.CountryDto
import org.restcountries.api.features.death.domain
import org.restcountries.api.features.death.dto.DeathDto
import org.restcountries.api.features.population.domain
import org.restcountries.api.features.population.dto.PopulationDto
import org.restcountries.api.features.region.RegionController
import org.springframework.hateoas.CollectionModel
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.hateoas.server.mvc.linkTo
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@Suppress("unused")
@RestController
@RequestMapping("/api/countries")
class CountryController(private val countryService: CountryService) {
	@GetMapping
	fun findAll(): ResponseEntity<CollectionModel<CountryDto>> {
		val countries = countryService.findAll().map { it.domain() }

		countries.forEach { country ->
			country.add(linkTo<CountryController> { findByM49Code(country.m49Code) }.withSelfRel())
		}

		val collectionModel = CollectionModel.of(countries)
		collectionModel.add(linkTo<CountryController> { findAll() }.withSelfRel())

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}")
	fun findByM49Code(@PathVariable code: String): ResponseEntity<CountryDto> {
		val country = countryService.findByM49Code(code)?.domain() ?: throw ResponseStatusException(
			HttpStatus.NOT_FOUND, "no country found with code $code"
		)

		country.add(linkTo<CountryController> { findByM49Code(code) }.withSelfRel())
		country.add(linkTo<CountryController> { findAll() }.withRel(IanaLinkRelations.COLLECTION))
		country.add(linkTo<CountryController> { findPopulations(code) }.withRel(LinkRelations.POPULATIONS))
		country.add(linkTo<CountryController> { findAges(code) }.withRel(LinkRelations.AGES))
		country.add(linkTo<CountryController> { findBirths(code) }.withRel(LinkRelations.BIRTHS))
		country.add(linkTo<CountryController> { findDeaths(code) }.withRel(LinkRelations.DEATHS))
		country.add(linkTo<RegionController> { findByCode(country.subregionCode) }.withRel(LinkRelations.REGION))

		return ResponseEntity.ok().body(country)
	}

	@GetMapping("/{code}/populations")
	fun findPopulations(@PathVariable code: String): ResponseEntity<CollectionModel<PopulationDto>> {
		val populations = countryService.findPopulations(code).map { it.domain() }

		val collectionModel = CollectionModel.of(populations)
		collectionModel.add(linkTo<CountryController> { findPopulations(code) }.withSelfRel())
		collectionModel.add(linkTo<CountryController> { findByM49Code(code) }.withRel(LinkRelations.COUNTRY))

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}/ages")
	fun findAges(@PathVariable code: String): ResponseEntity<CollectionModel<AgeDto>> {
		val ages = countryService.findAges(code).map { it.domain() }

		val collectionModel = CollectionModel.of(ages)
		collectionModel.add(linkTo<CountryController> { findAges(code) }.withSelfRel())
		collectionModel.add(linkTo<CountryController> { findByM49Code(code) }.withRel(LinkRelations.COUNTRY))

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}/births")
	fun findBirths(@PathVariable code: String): ResponseEntity<CollectionModel<BirthDto>> {
		val data = countryService.findBirths(code).map { it.domain() }

		val collectionModel = CollectionModel.of(data)
		collectionModel.add(linkTo<CountryController> { findBirths(code) }.withSelfRel())
		collectionModel.add(linkTo<CountryController> { findByM49Code(code) }.withRel(LinkRelations.COUNTRY))

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}/deaths")
	fun findDeaths(@PathVariable code: String): ResponseEntity<CollectionModel<DeathDto>> {
		val data = countryService.findDeaths(code).map { it.domain() }

		val collectionModel = CollectionModel.of(data)
		collectionModel.add(linkTo<CountryController> { findDeaths(code) }.withSelfRel())
		collectionModel.add(linkTo<CountryController> { findByM49Code(code) }.withRel(LinkRelations.COUNTRY))

		return ResponseEntity.ok().body(collectionModel)
	}
}
