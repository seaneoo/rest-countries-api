package org.restcountries.api.features.death

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface DeathRepository : JpaRepository<Death, Int> {
	@Query(value = "SELECT * FROM deaths WHERE country_code = :countryCode", nativeQuery = true)
	fun findDeaths(countryCode: String): List<Death>
}
