package org.restcountries.api.features.death

import org.springframework.stereotype.Service

@Service
class DeathService(private val deathRepository: DeathRepository) {
	fun findDeaths(countryCode: String) = deathRepository.findDeaths(countryCode)

	fun saveAll(deaths: Iterable<Death>): List<Death> = deathRepository.saveAll(deaths)
}
