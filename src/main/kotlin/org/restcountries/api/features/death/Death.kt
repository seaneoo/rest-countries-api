package org.restcountries.api.features.death

import jakarta.persistence.*
import org.restcountries.api.features.country.Country
import org.restcountries.api.features.death.dto.DeathDto

@Entity
@Table(name = "deaths")
data class Death(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 4, nullable = false, unique = false) val deathYear: String,
	@Column(nullable = false, unique = false) val totalDeaths: Double,
	@Column(nullable = false, unique = false) val maleDeaths: Double,
	@Column(nullable = false, unique = false) val femaleDeaths: Double,
	@Column(nullable = false, unique = false) val deathRate: Double,
	@Column(nullable = false, unique = false) val lifeExpectancy: Double,
	@Column(nullable = false, unique = false) val maleLifeExpectancy: Double,
	@Column(nullable = false, unique = false) val femaleLifeExpectancy: Double,
	@ManyToOne @JoinColumn(
		name = "countryCode", referencedColumnName = "m49Code"
	) val country: Country
)

fun Death.domain() = DeathDto(
	year = this.deathYear,
	totalDeaths = this.totalDeaths,
	maleDeaths = this.maleDeaths,
	femaleDeaths = this.femaleDeaths,
	deathRate = this.deathRate,
	lifeExpectancy = this.lifeExpectancy,
	maleLifeExpectancy = this.maleLifeExpectancy,
	femaleLifeExpectancy = this.femaleLifeExpectancy,
	m49Code = this.country.m49Code
)
