package org.restcountries.api.features.death.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class DeathDto(
	val year: String,
	@JsonProperty("total_deaths") val totalDeaths: Double,
	@JsonProperty("male_deaths") val maleDeaths: Double,
	@JsonProperty("female_deaths") val femaleDeaths: Double,
	@JsonProperty("death_rate") val deathRate: Double,
	@JsonProperty("life_expectancy") val lifeExpectancy: Double,
	@JsonProperty("male_life_expectancy") val maleLifeExpectancy: Double,
	@JsonProperty("female_life_expectancy") val femaleLifeExpectancy: Double,
	@JsonProperty("country_m49_code") val m49Code: String
) : RepresentationModel<DeathDto>()
