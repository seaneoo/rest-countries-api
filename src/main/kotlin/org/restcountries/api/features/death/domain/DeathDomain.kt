package org.restcountries.api.features.death.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.restcountries.api.features.country.Country
import org.restcountries.api.features.death.Death

@Serializable
data class DeathDomain(
	@SerialName("country_code") val countryCode: String,
	val year: String,
	@SerialName("total_deaths") val totalDeaths: String,
	@SerialName("male_deaths") val maleDeaths: String,
	@SerialName("female_deaths") val femaleDeaths: String,
	@SerialName("death_rate") val deathRate: String,
	@SerialName("life_expectancy") val lifeExpectancy: String,
	@SerialName("male_life_expectancy") val maleLifeExpectancy: String,
	@SerialName("female_life_expectancy") val femaleLifeExpectancy: String
)

fun DeathDomain.entity(country: Country) = Death(
	deathYear = this.year,
	totalDeaths = this.totalDeaths.toDoubleOrNull() ?: 0.0,
	maleDeaths = this.maleDeaths.toDoubleOrNull() ?: 0.0,
	femaleDeaths = this.femaleDeaths.toDoubleOrNull() ?: 0.0,
	deathRate = this.deathRate.toDoubleOrNull() ?: 0.0,
	lifeExpectancy = this.lifeExpectancy.toDoubleOrNull() ?: 0.0,
	maleLifeExpectancy = this.maleLifeExpectancy.toDoubleOrNull() ?: 0.0,
	femaleLifeExpectancy = femaleLifeExpectancy.toDoubleOrNull() ?: 0.0,
	country = country
)
