package org.restcountries.api.features.birth.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.restcountries.api.features.birth.Birth
import org.restcountries.api.features.country.Country

@Serializable
data class BirthDomain(
	@SerialName("country_code") val countryCode: String,
	val year: String,
	@SerialName("total_births") val totalBirths: String,
	@SerialName("birth_rate") val birthRate: String,
	@SerialName("fertility_rate") val fertilityRate: String,
	@SerialName("mean_childbearing_age") val meanChildbearingAge: String,
	@SerialName("sex_ratio") val sexRatio: String
)

fun BirthDomain.entity(country: Country) = Birth(
	birthYear = this.year,
	totalBirths = this.totalBirths.toDoubleOrNull() ?: 0.0,
	birthRate = this.birthRate.toDoubleOrNull() ?: 0.0,
	fertilityRate = this.fertilityRate.toDoubleOrNull() ?: 0.0,
	meanChildbearingAge = this.meanChildbearingAge.toDoubleOrNull() ?: 0.0,
	sexRatio = this.sexRatio.toDoubleOrNull() ?: 0.0,
	country = country
)
