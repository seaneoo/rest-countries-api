package org.restcountries.api.features.birth.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.restcountries.api.features.death.dto.DeathDto
import org.springframework.hateoas.RepresentationModel

data class BirthDto(
	val year: String,
	@JsonProperty("total_births") val totalBirths: Double,
	@JsonProperty("birth_rate") val birthRate: Double,
	@JsonProperty("fertility_rate") val fertilityRate: Double,
	@JsonProperty("mean_childbearing_age") val meanChildbearingAge: Double,
	@JsonProperty("sex_ratio") val sexRatio: Double,
	@JsonProperty("country_m49_code") val m49Code: String
) : RepresentationModel<DeathDto>()
