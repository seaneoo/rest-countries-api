package org.restcountries.api.features.birth

import org.springframework.stereotype.Service

@Service
class BirthService(private val birthRepository: BirthRepository) {
	fun findBirths(countryCode: String) = birthRepository.findBirths(countryCode)

	fun saveAll(births: Iterable<Birth>): List<Birth> = birthRepository.saveAll(births)
}
