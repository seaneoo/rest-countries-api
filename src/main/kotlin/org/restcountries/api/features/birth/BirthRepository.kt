package org.restcountries.api.features.birth

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface BirthRepository : JpaRepository<Birth, Int> {
	@Query(value = "SELECT * FROM births WHERE country_code = :countryCode", nativeQuery = true)
	fun findBirths(countryCode: String): List<Birth>
}
