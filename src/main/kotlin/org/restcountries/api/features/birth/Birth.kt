package org.restcountries.api.features.birth

import jakarta.persistence.*
import org.restcountries.api.features.birth.dto.BirthDto
import org.restcountries.api.features.country.Country

@Entity
@Table(name = "births")
data class Birth(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 4, nullable = false, unique = false) val birthYear: String,
	@Column(nullable = false, unique = false) val totalBirths: Double,
	@Column(nullable = false, unique = false) val birthRate: Double,
	@Column(nullable = false, unique = false) val fertilityRate: Double,
	@Column(nullable = false, unique = false) val meanChildbearingAge: Double,
	@Column(nullable = false, unique = false) val sexRatio: Double,
	@ManyToOne @JoinColumn(
		name = "countryCode", referencedColumnName = "m49Code"
	) val country: Country
)

fun Birth.domain() = BirthDto(
	year = this.birthYear,
	totalBirths = this.totalBirths,
	birthRate = this.birthRate,
	fertilityRate = this.fertilityRate,
	meanChildbearingAge = this.meanChildbearingAge,
	sexRatio = this.sexRatio,
	m49Code = this.country.m49Code
)
