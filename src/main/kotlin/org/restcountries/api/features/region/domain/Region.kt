package org.restcountries.api.features.region.domain

import kotlinx.serialization.Serializable
import org.restcountries.api.features.region.Region

@Serializable
data class SubregionDomain(
	val code: String, val name: String
)

@Serializable
data class RegionDomain(
	val code: String, val name: String, val subregions: List<SubregionDomain>
)

fun RegionDomain.entity() = Region(m49Code = this.code, name = this.name)

fun SubregionDomain.entity(parentRegionCode: String) =
	Region(m49Code = this.code, name = this.name, parentRegionCode = parentRegionCode)
