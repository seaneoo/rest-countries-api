package org.restcountries.api.features.region

import org.restcountries.api.LinkRelations
import org.restcountries.api.features.country.CountryController
import org.restcountries.api.features.country.domain
import org.restcountries.api.features.country.dto.CountryDto
import org.restcountries.api.features.region.dto.RegionDto
import org.springframework.hateoas.CollectionModel
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.hateoas.server.mvc.linkTo
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@Suppress("unused")
@RestController
@RequestMapping("/api/regions")
class RegionController(private val regionService: RegionService) {
	@GetMapping
	fun findAll(): ResponseEntity<CollectionModel<RegionDto>> {
		val regions = regionService.findAll().map { it.domain() }

		regions.forEach { region ->
			region.add(linkTo<RegionController> { findByCode(region.m49Code) }.withSelfRel())
		}

		val collectionModel = CollectionModel.of(regions)
		collectionModel.add(linkTo<RegionController> { findAll() }.withSelfRel())

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}")
	fun findByCode(@PathVariable code: String): ResponseEntity<RegionDto> {
		val region = regionService.findByM49Code(code)?.domain() ?: throw ResponseStatusException(
			HttpStatus.NOT_FOUND, "no region found with code $code"
		)

		region.add(linkTo<RegionController> { findByCode(code) }.withSelfRel())
		region.add(linkTo<RegionController> { findAll() }.withRel(IanaLinkRelations.COLLECTION))
		if (region.parentRegionCode == null) {
			region.add(linkTo<RegionController> { findSubregionsByCode(code) }.withRel(LinkRelations.SUBREGIONS))
		}
		if (region.parentRegionCode != null) {
			region.add(linkTo<RegionController> { findCountriesByCode(code) }.withRel(LinkRelations.COUNTRIES))
		}

		return ResponseEntity.ok().body(region)
	}

	@GetMapping("/{code}/subregions")
	fun findSubregionsByCode(@PathVariable code: String): ResponseEntity<CollectionModel<RegionDto>> {
		val regions = regionService.findSubregions(code).map { it.domain() }
			.ifEmpty { throw ResponseStatusException(HttpStatus.NOT_FOUND, "no region found with code $code") }

		regions.forEach { region ->
			region.add(linkTo<RegionController> { findByCode(region.m49Code) }.withSelfRel())
			region.add(linkTo<RegionController> { findAll() }.withRel(IanaLinkRelations.COLLECTION))
			region.add(linkTo<RegionController> { findCountriesByCode(region.m49Code) }.withRel(LinkRelations.COUNTRIES))
		}

		val collectionModel = CollectionModel.of(regions)
		collectionModel.add(linkTo<RegionController> { findSubregionsByCode(code) }.withSelfRel())
		collectionModel.add(linkTo<RegionController> { findByCode(code) }.withRel(LinkRelations.REGION))

		return ResponseEntity.ok().body(collectionModel)
	}

	@GetMapping("/{code}/countries")
	fun findCountriesByCode(@PathVariable code: String): ResponseEntity<CollectionModel<CountryDto>> {
		val countries = regionService.findCountries(code).map { it.domain() }
			.ifEmpty { throw ResponseStatusException(HttpStatus.NOT_FOUND, "no subregion found with code $code") }

		countries.forEach { country ->
			country.add(linkTo<CountryController> { findByM49Code(country.m49Code) }.withSelfRel())
			country.add(linkTo<CountryController> { findAll() }.withRel(IanaLinkRelations.COLLECTION))
			country.add(linkTo<CountryController> { findPopulations(country.m49Code) }.withRel(LinkRelations.POPULATIONS))
			country.add(linkTo<CountryController> { findAges(country.m49Code) }.withRel(LinkRelations.AGES))
			country.add(linkTo<CountryController> { findBirths(country.m49Code) }.withRel(LinkRelations.BIRTHS))
			country.add(linkTo<CountryController> { findDeaths(country.m49Code) }.withRel(LinkRelations.DEATHS))
			country.add(linkTo<RegionController> { findByCode(country.subregionCode) }.withRel(LinkRelations.REGION))
		}

		val collectionModel = CollectionModel.of(countries)
		collectionModel.add(linkTo<RegionController> { findCountriesByCode(code) }.withSelfRel())
		collectionModel.add(linkTo<RegionController> { findByCode(code) }.withRel(LinkRelations.REGION))

		return ResponseEntity.ok().body(collectionModel)
	}
}
