package org.restcountries.api.features.region

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface RegionRepository : JpaRepository<Region, Int> {
	fun findByM49Code(code: String): Optional<Region>

	@Query(
		value = "SELECT * FROM regions WHERE parent_region_code IS NOT NULL AND parent_region_code = :parentRegionCode",
		nativeQuery = true
	)
	fun findSubregions(parentRegionCode: String): List<Region>
}
