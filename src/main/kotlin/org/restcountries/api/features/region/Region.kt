package org.restcountries.api.features.region

import jakarta.persistence.*
import org.restcountries.api.features.region.dto.RegionDto

@Entity
@Table(name = "regions")
data class Region(
	@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Int? = null,
	@Column(length = 3, nullable = false, unique = true) val m49Code: String,
	@Column(length = 100, nullable = false, unique = true) val name: String,
	@Column(length = 3, nullable = true, unique = false) val parentRegionCode: String? = null
)

fun Region.domain() = RegionDto(m49Code = this.m49Code, name = this.name, parentRegionCode = this.parentRegionCode)
