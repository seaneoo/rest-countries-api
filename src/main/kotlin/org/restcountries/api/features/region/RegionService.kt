package org.restcountries.api.features.region

import org.restcountries.api.features.country.Country
import org.restcountries.api.features.country.CountryService
import org.springframework.stereotype.Service
import kotlin.jvm.optionals.getOrNull

@Service
class RegionService(
	private val regionRepository: RegionRepository, private val countryService: CountryService
) {
	fun findAll(): List<Region> = regionRepository.findAll()

	fun findByM49Code(code: String): Region? = regionRepository.findByM49Code(code).getOrNull()

	fun findSubregions(parentRegionCode: String): List<Region> = regionRepository.findSubregions(parentRegionCode)

	fun findCountries(subregionCode: String): List<Country> = countryService.findCountries(subregionCode)

	fun saveAll(regions: Iterable<Region>): List<Region> = regionRepository.saveAll(regions)
}
