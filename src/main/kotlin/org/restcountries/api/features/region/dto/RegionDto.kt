package org.restcountries.api.features.region.dto

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class RegionDto(
	@JsonProperty("m49_code") val m49Code: String,
	val name: String,
	@JsonProperty("parent_region_code") val parentRegionCode: String? = null
) : RepresentationModel<RegionDto>()
