package org.restcountries.api

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class RestCountriesApplication {
	@Suppress("unused")
	@Bean
	fun openApi(): OpenAPI {
		val contact = Contact().email("help@restcountries.org")
		val license = License().name("GNU Affero General Public License Version 3")
			.url("https://gitlab.com/seaneoo/rest-countries-api/-/raw/main/LICENSE")
		val info = Info().title("REST Countries API").version("1.0")
			.description("Comprehensive information on regions and countries worldwide in a easy-to-use REST API.")
			.contact(contact).license(license)
		return OpenAPI().info(info)
	}
}

fun main(args: Array<String>) {
	runApplication<RestCountriesApplication>(*args)
}
