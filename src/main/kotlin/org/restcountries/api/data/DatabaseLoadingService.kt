package org.restcountries.api.data

import org.restcountries.api.features.age.AgeService
import org.restcountries.api.features.age.domain.AgeDomain
import org.restcountries.api.features.age.domain.entity
import org.restcountries.api.features.birth.BirthService
import org.restcountries.api.features.birth.domain.BirthDomain
import org.restcountries.api.features.birth.domain.entity
import org.restcountries.api.features.country.Country
import org.restcountries.api.features.country.CountryService
import org.restcountries.api.features.country.domain.CountryDomain
import org.restcountries.api.features.country.domain.entity
import org.restcountries.api.features.death.DeathService
import org.restcountries.api.features.death.domain.DeathDomain
import org.restcountries.api.features.death.domain.entity
import org.restcountries.api.features.population.PopulationService
import org.restcountries.api.features.population.domain.PopulationDomain
import org.restcountries.api.features.population.domain.entity
import org.restcountries.api.features.region.RegionService
import org.restcountries.api.features.region.domain.RegionDomain
import org.restcountries.api.features.region.domain.entity
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class DatabaseLoadingService(
	private val regionService: RegionService,
	private val countryService: CountryService,
	private val populationService: PopulationService,
	private val ageService: AgeService,
	private val birthService: BirthService,
	private val deathService: DeathService,
	private val logger: Logger = LoggerFactory.getLogger(DatabaseLoadingService::class.java)
) {
	private lateinit var countryCache: List<Country>

	fun collectRegions(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<RegionDomain>>("regions.json")

		val regions = rawData.map { domain -> domain.entity() }
		val subregions = rawData.flatMap { parent -> parent.subregions.map { child -> child.entity(parent.code) } }

		val saved = regionService.saveAll(regions + subregions)
		logger.info("Saved ${saved.size} entries to the \"regions\" table.")
		return this
	}

	fun collectCountries(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<CountryDomain>>("countries.json")
		val data = rawData.map { it.entity() }
		countryCache = countryService.saveAll(data)
		logger.info("Saved ${countryCache.size} entries to the \"countries\" table.")
		return this
	}

	fun collectPopulations(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<PopulationDomain>>("populations.json")

		val domainCountryMap = rawData.mapNotNull { domain ->
			val country = countryCache.firstOrNull { country -> country.m49Code.equals(domain.countryCode, true) }
			if (country != null) domain to country else null
		}.toMap()

		val data = domainCountryMap.map { (domain, country) -> domain.entity(country) }

		val saved = populationService.saveAll(data)
		logger.info("Saved ${saved.size} entries to the \"populations\" table.")
		return this
	}

	fun collectAges(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<AgeDomain>>("ages.json")

		val domainCountryMap = rawData.mapNotNull { domain ->
			val country = countryCache.firstOrNull { country -> country.m49Code.equals(domain.countryCode, true) }
			if (country != null) domain to country else null
		}.toMap()

		val data = domainCountryMap.map { (domain, country) -> domain.entity(country) }

		val saved = ageService.saveAll(data)
		logger.info("Saved ${saved.size} entries to the \"ages\" table.")
		return this
	}

	fun collectBirths(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<BirthDomain>>("births.json")

		val domainCountryMap = rawData.mapNotNull { domain ->
			val country = countryCache.firstOrNull { country -> country.m49Code.equals(domain.countryCode, true) }
			if (country != null) domain to country else null
		}.toMap()

		val data = domainCountryMap.map { (domain, country) -> domain.entity(country) }

		val saved = birthService.saveAll(data)
		logger.info("Saved ${saved.size} entries to the \"births\" table.")
		return this
	}

	fun collectDeaths(): DatabaseLoadingService {
		val rawData = DataParsingUtils.instance.openAndDecode<List<DeathDomain>>("deaths.json")

		val domainCountryMap = rawData.mapNotNull { domain ->
			val country = countryCache.firstOrNull { country -> country.m49Code.equals(domain.countryCode, true) }
			if (country != null) domain to country else null
		}.toMap()

		val data = domainCountryMap.map { (domain, country) -> domain.entity(country) }

		val saved = deathService.saveAll(data)
		logger.info("Saved ${saved.size} entries to the \"deaths\" table.")
		return this
	}
}
