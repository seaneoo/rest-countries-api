package org.restcountries.api.data

import org.restcountries.api.ApplicationAttributes
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.time.ZoneId
import java.time.ZonedDateTime

@Suppress("unused")
@Component
class DatabaseLoader(
	private val databaseLoadingService: DatabaseLoadingService
) : CommandLineRunner {
	override fun run(vararg args: String?) {
		databaseLoadingService.collectRegions().collectCountries().collectPopulations().collectAges().collectBirths()
			.collectDeaths()
		// Update the LastModified timestamp
		ApplicationAttributes.lastModified = ZonedDateTime.now(ZoneId.of("GMT"))
	}
}
