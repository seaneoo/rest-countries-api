package org.restcountries.api.data

import kotlinx.serialization.json.Json

class DataParsingUtils private constructor() {
	companion object {
		val instance by lazy { DataParsingUtils() }
	}

	inline fun <reified T : Any> openAndDecode(fileStr: String): T {
		val bufferedReader = DataParsingUtils::class.java.getResourceAsStream("/$fileStr")?.bufferedReader()
		val textContent =
			bufferedReader?.readText() ?: throw RuntimeException("Could not read text content from file \"$fileStr\".")
		return Json.decodeFromString<T>(textContent)
	}
}
