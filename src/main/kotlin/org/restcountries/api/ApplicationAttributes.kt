package org.restcountries.api

import org.restcountries.api.ApplicationAttributes.lastModified
import java.time.ZonedDateTime

/**
 * Global application attributes
 *
 * @property lastModified Keep track of when the data was generated for caching purposes.
 */
object ApplicationAttributes {
	lateinit var lastModified: ZonedDateTime
}
