package org.restcountries.api

object LinkRelations {
	const val REGION = "region"
	const val SUBREGIONS = "subregions"
	const val COUNTRIES = "countries"
	const val COUNTRY = "country"
	const val POPULATIONS = "populations"
	const val AGES = "ages"
	const val BIRTHS = "births"
	const val DEATHS = "deaths"
}
