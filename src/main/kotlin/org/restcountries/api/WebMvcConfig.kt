package org.restcountries.api

import org.restcountries.api.interceptors.LastModifiedInterceptor
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Suppress("unused")
@Configuration
class WebMvcConfig : WebMvcConfigurer {
	override fun addInterceptors(registry: InterceptorRegistry) {
		super.addInterceptors(registry)
		registry.addInterceptor(LastModifiedInterceptor()).addPathPatterns("/api/**")
	}
}
