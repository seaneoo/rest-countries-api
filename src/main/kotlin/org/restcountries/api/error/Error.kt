package org.restcountries.api.error

import java.time.Instant

data class Error(val status: Int, val reason: String?, val timestamp: Instant = Instant.now())
