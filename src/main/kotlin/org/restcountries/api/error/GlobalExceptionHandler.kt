package org.restcountries.api.error

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@Suppress("unused")
@ControllerAdvice
@RestController
class GlobalExceptionHandler {
	@ExceptionHandler(ResponseStatusException::class)
	@ResponseBody
	fun handleResponseStatusException(exception: ResponseStatusException): ResponseEntity<Error> {
		val statusCode = exception.statusCode
		val error = Error(statusCode.value(), exception.reason)
		return ResponseEntity.status(statusCode).body(error)
	}
}
