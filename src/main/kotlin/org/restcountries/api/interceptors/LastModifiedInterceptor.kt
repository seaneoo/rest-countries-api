package org.restcountries.api.interceptors

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.restcountries.api.ApplicationAttributes
import org.springframework.http.HttpHeaders
import org.springframework.web.servlet.HandlerInterceptor

class LastModifiedInterceptor : HandlerInterceptor {
	override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
		response.setDateHeader(HttpHeaders.LAST_MODIFIED, ApplicationAttributes.lastModified.toInstant().toEpochMilli())
		return super.preHandle(request, response, handler)
	}
}
