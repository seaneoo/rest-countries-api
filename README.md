# REST Countries API

Comprehensive information on regions and countries worldwide in an easy-to-use REST API.

## Data

This API features the following data:

- 22 regions / subregions.
- 247 countries.
- Population, age, birth, and death data from 1950 to 2021 for each country.

All statistics¹ are obtained from the United Nations.

### Sources

- https://unstats.un.org/unsd/methodology/m49/
- https://population.un.org/wpp/
- https://flagcdn.com/

---

¹ Non-numerical data such as flags, alternative names, etc. are obtained from various sources.

## License

Copyright (C) 2024 Sean O'Connor.

Released under [GNU Affero General Public License Version 3](LICENSE).
